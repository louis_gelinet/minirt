NAME    = miniRT

HEAD    = includes

SRCDIR  = srcs/

LIB             = ./lib/

FILES   =       ../minirt.c \
                parse.c \
				objs_init.c \
				get_info.c \
				get_info_utils.c \
				inits.c \

SRCS    = $(addprefix $(SRCDIR), $(FILES))

OBJS    = ${SRCS:.c=.o}

CC              = gcc -g

RM              = rm -f

CFLAGS  = -Wall -Wextra -Werror  -I $(HEAD) -D NUM_THREADS=$(NUM_THREADS)

FLAGS = -L $(LIB)libft -lft

MACOS_MACRO = -D MACOS -D ESCAPE=53

LINUX_MACRO = -D LINUX -D ESCAPE=65307

MACOS_FLAGS = -L $(LIB) -lmlx -framework OpenGL -framework AppKit 

LINUX_FLAGS = -L $(LIB) -lmlx -lmlx_Linux  -lm -lX11 -lXext -lpthread

UNAME := $(shell uname)

ifeq ($(UNAME),Darwin)
        NUM_THREADS = $(shell sysctl -n hw.ncpu)
        CFLAGS += $(MACOS_MACRO)
        FLAGS += $(MACOS_FLAGS)
endif
ifeq ($(UNAME),Linux)
        NUM_THREADS = $(shell nproc --all)
        CFLAGS += $(LINUX_MACRO)
        FLAGS += $(LINUX_FLAGS)
endif

${NAME}:	${OBJS}
		make -C $(LIB)libft
		${CC} ${CFLAGS} $(OBJS) $(FLAGS) -o ${NAME}

all:            ${NAME}

clean:
		make clean -C $(LIB)libft
		${RM} ${OBJS}

fclean:         clean
		make fclean -C $(LIB)libft
		${RM} ${NAME}

re:                     fclean all

PHONY:          all clean fclean re
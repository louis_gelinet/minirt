/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minirt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/23 15:29:13 by lgelinet          #+#    #+#             */
/*   Updated: 2021/03/05 17:22:13 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINIRT_H
# define MINIRT_H
# include "mlx.h"
# include "structs.h"
# include <stdio.h>
# include <stdlib.h>
# include "libft.h"
# include <math.h>
# define WK 13
# define AK 0
# define DK 2
# define SK 1
# define UP 126
# define DOWN 125
# define LEFT 123
# define RIGHT 124
# define SPACE 49
# define RED 255<<16
# define ARRAY_L 1000
# define ARRAY_W 1000

int				ft_plane_init(char *str, t_obj *ret);
int				ft_sphere_init(char *str, t_obj *ret);
int				ft_cylinder_init(char *str, t_obj *ret);
int				ft_triangle_init(char *str, t_obj *ret);
int				ft_square_init(char *str, t_obj *ret);
int						parse_img(int fd, t_img **ret);
int ft_new_img(void* mlx, char *file, t_img *ret);
int     ft_new_obj(char *str, t_img *img);
int		atof_point(char *str, float *tochange);
int		atoi_point(char *str, int *tochange);
int		ft_get_color(char *str, int *color);
int		ft_get_vec(char *str, t_vec *ret);
int		ft_get_light(char *str, t_light *ret);
int 	ft_get_cam(char *str, t_cam *ret);
int		ft_get_res(char *str, t_img *ret);
int 	ft_get_amb(char *str, t_ambient_light *ret);

#endif
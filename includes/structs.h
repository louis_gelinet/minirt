/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/24 15:27:44 by lgelinet          #+#    #+#             */
/*   Updated: 2021/03/05 17:00:12 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTS_H
# define STRUCTS_H
# include "minirt.h"

typedef struct 	u_vec
{
	float x;
	float y;
	float z;
}				t_vec;

typedef	struct 	s_amb
{
	float	lum;
	int  color;
}				t_ambient_light;

typedef	struct	s_cam
{
	t_vec	pov;
	t_vec	ori;
	int		fov;
}				t_cam;

typedef	struct	s_light
{
	t_vec	pos;
	float	lum;
	int 	color;
}				t_light;

typedef  struct s_obj
{
	char	id[3];
	t_vec	coor;
	t_vec	coor2;
	t_vec	coor3;
	float	dia;
	float	hei;
	t_vec	ori;
	int 	color;
	struct s_obj	*next;
}				t_obj;

typedef	struct		s_img
{
	int		**screen;
	// TOFREE
	t_obj	*objptr;
	t_light	light;
	t_cam	cam;
	int 	withd;
	int		hei;
	t_ambient_light	amb;
	void	*lmlxptr;
	void	*winptr;
}					t_img;




 #endif
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/25 16:06:39 by lgelinet          #+#    #+#             */
/*   Updated: 2021/03/03 14:15:18 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

float   ft_atof(char *str)
{
    int i;
    int power;
    int j;
	int buff;
    
    i = 0;
    j = 0;
    power = 0;
    while (!ft_isdigit(str[i]))
        i++;
    while (ft_isdigit(str[i]))
        i++;
    if (str[i] == '.')
    {
        j = i + 1;
        while(ft_isdigit(str[++i]))
            power++;
    }
	buff = ft_atoi(str);
	i = buff < 0 ? -1 : 1;
    return (i * ( i * ft_atoi(str) + (float)ft_atoi(&str[j])/ ft_pow(10,power)));
}
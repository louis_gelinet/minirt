/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/25 19:52:23 by lgelinet          #+#    #+#             */
/*   Updated: 2021/02/02 11:19:19 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static		int	ft_iswhitespace(char c)
{
	if (c == ' ' || c == '\f')
		return (1);
	if (c == '\r' || c == '\n')
		return (1);
	if (c == '\t' || c == '\v')
		return (1);
	return (0);
}

int				ft_atoi(char *s)
{
	int		buff;
	int		res;
	int		i;

	i = 0;
	res = 0;
	buff = 1;
	while (ft_iswhitespace(s[i]))
		i++;
	if ((s[i] == '-' || s[i] == '+') && ft_isdigit((s[i + 1])))
	{
		buff *= (s[i] == '-' ? -1 : 1);
		i++;
	}
	while (ft_isdigit(s[i]))
	{
		res = res * 10 + (s[i] - '0');
		i++;
	}
	return (buff * res);
}

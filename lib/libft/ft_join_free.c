/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_join_free.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/25 13:25:55 by lgelinet          #+#    #+#             */
/*   Updated: 2021/02/25 13:26:10 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_join_free_free(char *s1, char *s2, int freed)
{
	if (freed == 3 || freed == 1)
	{
		free(s1);
		s1 = NULL;
	}
	if (freed == 3 || freed == 2)
	{
		free(s2);
		s2 = NULL;
	}
}
char		*ft_join_free(char *s1, char *s2, int freed)
{
	char		*str;
	size_t		i;
	size_t		j;

	if (!s1)
		return (ft_strdup(s2));
	str = (char *)malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 1));
	if (!str)
		return (NULL);
	i = -1;
	while (s1[++i] && s1)
		str[i] = s1[i];
	j = i;
	i = -1;
	while (s2[++i] && s2)
		str[j + i] = s2[i];
	str[j + i] = '\0';
	
	s1 = NULL;
	return (str);
}
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/01 17:49:08 by lgelinet          #+#    #+#             */
/*   Updated: 2020/12/01 18:51:49 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **alst, t_list *newl)
{
	t_list	*temp;

	if (newl && *alst)
	{
		temp = *alst;
		while (temp->next != NULL)
			temp = temp->next;
		temp->next = newl;
		newl->next = NULL;
	}
	if (newl && *alst == NULL)
		*alst = newl;
	return ;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/25 20:44:15 by lgelinet          #+#    #+#             */
/*   Updated: 2021/02/18 15:09:37 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(void *s)
{
	char			*ptr;
	char			*dup;
	unsigned	int	i;

	i = 0;
	if (!s)
		return (ft_strdup("(null)"));
	ptr = (char *)s;
	dup = (char *)malloc(sizeof(char) * ft_strlen(ptr) + 1);
	if (dup == NULL)
		return (NULL);
	while (i < ft_strlen(ptr))
	{
		dup[i] = ptr[i];
		i++;
	}
	dup[i] = 0;
	return (dup);
}

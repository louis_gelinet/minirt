/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/25 20:58:17 by lgelinet          #+#    #+#             */
/*   Updated: 2021/01/08 14:21:45 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *subs, size_t len)
{
	unsigned int i;
	unsigned int j;

	i = 0;
	if (subs[i] == '\0')
		return ((char *)big);
	while (big[i] && i < len)
	{
		j = 0;
		if (big[i] == subs[j])
		{
			while (i + j < len && big[i + j] == subs[j])
			{
				j++;
				if (!subs[j])
					return ((char *)&big[i]);
			}
		}
		i++;
	}
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/25 20:40:24 by lgelinet          #+#    #+#             */
/*   Updated: 2020/11/27 16:14:22 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int	i;

	i = 0;
	if (*s == '\0' && !c)
		return ((char *)s);
	while (*s)
	{
		s++;
		i++;
	}
	while (*s != (char)c && i != 0)
	{
		s--;
		i--;
	}
	if (i == 0 && s[0] != (char)c)
		return (0);
	else
		return ((char *)s);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_doubletabset.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/24 13:32:01 by lgelinet          #+#    #+#             */
/*   Updated: 2021/02/24 14:00:37 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_doubletabintset(int **tab, int lenght, int withd)
{
	int i;
	int j;

	i = -1;
	tab = (int **)malloc(sizeof(int *) * (lenght));
	while (++i < lenght)
	{
		tab[i] = malloc (sizeof(int) * withd);
		j = -1;
		while (++j < withd)
			tab[i][j] = 0;
	}
	return (0);
}
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/26 15:02:53 by lgelinet          #+#    #+#             */
/*   Updated: 2021/02/22 14:32:36 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int			ft_itoa_size(int n)
{
	int				size;

	size = 0;
	if (n < 0 && n > -2147483648)
	{
		size++;
		n = -n;
	}
	else if (n == 0)
		return (1);
	else if (n == -2147483648)
		return (11);
	while (n != 0)
	{
		n /= 10;
		size++;
	}
	return (size);
}

char				*ft_itoa(int nb)
{
	char			*str;
	int				i;
	int				size;
	unsigned int	temp;
	int				n;

	n = (int)nb;
	size = ft_itoa_size(n);
	i = 1;
	if (!(str = (char *)malloc(sizeof(char) * size + 1)))
		return (NULL);
	str[size] = '\0';
	temp = n < 0 ? -n : n;
	if (temp == 0)
		str[0] = '0';
	while (temp >= 1)
	{
		str[size - i] = (temp % 10) + '0';
		temp /= 10;
		i++;
	}
	if (n < 0)
		*str = '-';
	return (str);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_front.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/25 20:20:57 by lgelinet          #+#    #+#             */
/*   Updated: 2020/11/25 20:21:21 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_front(t_list **alst, t_list *newl)
{
	if (!alst)
	{
		*alst = newl;
		newl->next = NULL;
	}
	else
	{
		newl->next = *alst;
		*alst = newl;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/25 19:48:20 by lgelinet          #+#    #+#             */
/*   Updated: 2020/12/01 18:22:02 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	signed char	operation;
	size_t		end;
	size_t		current;

	if (dest != src)
	{
		if (dest < src)
		{
			operation = 1;
			current = 0;
			end = n;
		}
		else
		{
			operation = -1;
			current = n - 1;
			end = -1;
		}
		while (current != end)
		{
			*(((char*)dest) + current) = *(((char*)src) + current);
			current += operation;
		}
	}
	return (dest);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/25 20:47:27 by lgelinet          #+#    #+#             */
/*   Updated: 2021/01/08 12:16:49 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	char	*pdest;
	char	*psrc;
	size_t	len;
	size_t	dst_len;

	if (!(pdest = (char *)ft_memchr(dst, '\0', size)))
		return (size + ft_strlen(src));
	psrc = (char *)src;
	pdest = (char *)dst;
	dst_len = ft_strlen(dst);
	len = dst_len + ft_strlen(psrc);
	pdest += dst_len;
	while (dst_len++ < size - 1 && *psrc)
		*pdest++ = *psrc++;
	*pdest = '\0';
	return (len);
}

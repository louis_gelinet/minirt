/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/02 14:52:52 by lgelinet          #+#    #+#             */
/*   Updated: 2021/02/25 14:06:04 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#define BUFFER_SIZE 64

int			ft_keeped_case(char **line, char *keeped, unsigned int n_occur)
{
	unsigned int	ret_len;

	*line = ft_substr(keeped, 0, n_occur - 1);
	ret_len = ft_strlen(keeped + n_occur) + 1;
	ft_memmove(keeped, keeped + n_occur, ret_len);
	return (1);
}

int			get_next_line(int fd, char **line)
{
	static	char		*keeped[4096];
	char				temp[BUFFER_SIZE + 1];
	int					ret;
	unsigned	int		n_occur;

	if (fd < 0 || !line || read(fd, temp, 0) < 0)
		return (-1);
	if (keeped[fd] && (n_occur = ft_rankchr(keeped[fd], '\n')))
		return (ft_keeped_case(line, keeped[fd], n_occur));
	while ((ret = read(fd, temp, BUFFER_SIZE)))
	{
		temp[ret] = '\0';
		keeped[fd] = ft_join_free(keeped[fd], temp, 1);
		if ((n_occur = ft_rankchr(keeped[fd], '\n')))
			return (ft_keeped_case(line, keeped[fd], n_occur));
	}
	if (keeped[fd])
	{
		*line = ft_strdup(keeped[fd]);
		free(keeped[fd]);
		keeped[fd] = NULL;
		return (ret);
	}
	*line = ft_strdup("");
	return (ret);
}

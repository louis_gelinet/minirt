/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minirt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/23 15:27:07 by lgelinet          #+#    #+#             */
/*   Updated: 2021/03/05 16:43:45 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./includes/minirt.h"
void	free_img(t_img *img)
{
	t_obj *temp;
	t_obj *temp2;
	
	temp = img->objptr;
	if (temp)
	{
		temp2= temp->next;
		while (temp2)
		{
			temp->next = temp2->next;
			free (temp2);
			temp2 = temp->next;
		}
			free (temp);
	}
	free (img);
	exit(1);
}
int		deal_key(int key ,t_img *img)
{

	if (key == ESCAPE)
		free_img(img);
	printf("q = %d\n", key);
	return (1);
}

int		main(void)
{
	t_img *ret = NULL;

	ret = (t_img *)malloc(sizeof(t_img));
	if (!ft_new_img(mlx_init(), "examples/test.rt", ret))
		free_img(ret);
	printf("R = %d %d\nA =%f %x\nc = %f,%f,%f %f,%f,%f %d\nl = %f,%f,%f %f %x\n%s =%f,%f,%f %f %x\n%s = %f,%f,%f %f,%f,%f %f %x", ret->withd, ret->hei, 
	ret->amb.lum,ret->amb.color,\
	ret->cam.pov.x,ret->cam.pov.y,ret->cam.pov.z,ret->cam.ori.x,ret->cam.ori.y,ret->cam.ori.z, ret->cam.fov ,\
	ret->light.pos.x,ret->light.pos.y,ret->light.pos.z, ret->light.lum, ret->light.color, \
	ret->objptr->next->id, ret->objptr->next->coor.x,ret->objptr->next->coor.y,ret->objptr->next->coor.z,ret->objptr->next->dia,ret->objptr->next->color, \
	ret->objptr->id, ret->objptr->coor.x,ret->objptr->coor.y,ret->objptr->coor.z,ret->objptr->ori.x,ret->objptr->ori.y,ret->objptr->ori.z, ret->objptr->hei,ret->objptr->color);
	//printf("%f", ret->objptr->next->hei);
	mlx_key_hook(ret->winptr, deal_key, ret);
	mlx_loop(ret->lmlxptr);
}

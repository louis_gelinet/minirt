/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_info.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/26 14:56:37 by lgelinet          #+#    #+#             */
/*   Updated: 2021/03/05 16:59:38 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minirt.h"

int		ft_get_light(char *str, t_light *ret)
{
	int i;
	int iprec;

	i = 0;
	i += ft_get_vec(str, &(ret->pos));
	iprec = i;
	i += atof_point(&str[i], &(ret->lum));
	if (iprec == 0 || iprec == i || !ft_get_color(&str[i], &ret->color))
		return (0 * ft_putstr_fd("LIGHT INIT ERROR -> Check format\n", 1));
	return (1);
}

int		ft_get_cam(char *str, t_cam *ret)
{
	int i;
	int iprec;

	i = 0;
	i += ft_get_vec(str, &ret->pov);
	iprec = i;
	i += ft_get_vec(&str[i], &ret->ori);
	if (!iprec || iprec == i || !atoi_point(&str[i], &ret->fov))
		return (0 * ft_putstr_fd("CAM INIT ERROR -> Check format\n", 1));
	return (1);
}

int		ft_get_res(char *str, t_img *ret)
{
	int i;
	int iprec;

	i = 0;
	i += atoi_point(str, &ret->withd);
	iprec = i;
	i += atoi_point(&str[i], &ret->hei);
	if (!iprec || iprec == i || ret->hei < 1 || ret->withd < 1)
		return (0 * ft_putstr_fd("RES INIT ERROR -> Check format\n", 1));
	return (1);
}

int		ft_get_amb(char *str, t_ambient_light *ret)
{
	int i;

	i = 0;
	i += atof_point(str, &ret->lum);
	if (!i || !ft_get_color(&str[i], &ret->color))
		return (0 * ft_putstr_fd("AMB INIT ERROR -> Check format\n", 1));
	return (1);
}

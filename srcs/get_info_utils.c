/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_info_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 16:54:08 by lgelinet          #+#    #+#             */
/*   Updated: 2021/03/05 16:58:59 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minirt.h"

int		atof_point(char *str, float *tochange)
{
	int i;
	int iprec;

	*tochange = ft_atof(str);
	i = 0;
	while (!ft_strrchr("-.0123456789", str[i]) && str[i])
		i++;
	iprec = i;
	while (ft_strrchr("-.0123456789", str[i]) && str[i])
		i++;
	if (iprec == i)
		return (0);
	return (i + 1);
}

int		atoi_point(char *str, int *tochange)
{
	int i;
	int iprec;

	i = 0;
	*tochange = ft_atoi(str);
	while (!ft_isdigit(str[i]) && str[i])
		i++;
	iprec = i;
	while (ft_isdigit(str[i]))
		i++;
	if (iprec == i)
		return (0);
	return (i + 1);
}

int		ft_get_color(char *str, int *color)
{
	int i;
	int colthird;

	i = 0;
	*color = 0;
	colthird = 0;
	i += atoi_point(str, &colthird);
	*color += colthird ? colthird << 16 : 0;
	i += atoi_point(&str[i], &colthird);
	*color += colthird ? colthird << 8 : 0;
	atoi_point(&str[i], &colthird);
	*color += colthird;
	return (1);
}

int		ft_get_vec(char *str, t_vec *ret)
{
	int i;
	int iprec;
	int iprec2;

	i = 0;
	i += atof_point(str, &(ret->x));
	iprec2 = i;
	i += atof_point(&str[i], &(ret->y));
	iprec = i;
	i += atof_point(&str[i], &(ret->z));
	if (!iprec2 || iprec == iprec2 || i == iprec)
		return (0);
	return (i);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inits.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/25 16:27:04 by lgelinet          #+#    #+#             */
/*   Updated: 2021/03/05 15:29:59 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minirt.h"

int			ft_new_img(void *mlx, char *file, t_img *ret)
{ 
	if (!parse_img(open(file, O_RDONLY), &ret))
		return (0);
//	printf("[1]\n");
	ret->lmlxptr = mlx;
	ret->winptr = mlx_new_window(mlx, ret->withd, ret->hei, "test");
	return (1);
}


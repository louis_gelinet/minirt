/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objs_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/25 17:09:02 by lgelinet          #+#    #+#             */
/*   Updated: 2021/03/05 17:13:01 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minirt.h"

int				ft_plane_init(char *str, t_obj *ret)
{
	int i;
	int iprec;

	i = 0;
	i += ft_get_vec(str, &ret->coor);
	iprec = i;
	i += ft_get_vec(&str[i], &ret->ori);
	if (!iprec || iprec == i || !ft_get_color(&str[i], &ret->color))
		return (0);
	return (1);
}

int				ft_sphere_init(char *str, t_obj *ret)
{
	int i;
	int iprec;

	i = 0;
	printf("sphere\n");
	i += ft_get_vec(str, &ret->coor);
	iprec = i;
	i += atof_point(&str[i], &ret->dia);
	if (!iprec || iprec == i || !ft_get_color(&str[i], &ret->color))
		return (0);
	return (1);
}

int				ft_cylinder_init(char *str, t_obj *ret)
{
	int i;
	int iprec;
	int iprec2;
	int iprec3;

	i = 0;
	i += ft_get_vec(str, &ret->coor);
	iprec3 = i;
	i += ft_get_vec(&str[i], &ret->ori);
	iprec2 = i;
	i += atof_point(&str[i], &ret->dia);
	iprec = i;
	i += atof_point(&str[i], &ret->hei);
	if (!(ft_get_color(&str[i], &ret->color)))
		return (0);
	if (!iprec3 || iprec3 == iprec2 || iprec2 == iprec || iprec == i)
		return (0);
	return (1);
}

int				ft_triangle_init(char *str, t_obj *ret)
{
	int i;
	int iprec;
	int iprec2;

	i = 0;
	i += ft_get_vec(str, &ret->coor);
	iprec2 = i;
	i += ft_get_vec(&str[i], &ret->coor2);
	iprec = i;
	i += ft_get_vec(&str[i], &ret->coor3);
	if (!(ft_get_color(&str[i], &ret->color)))
		return (0);
	if (!iprec2 || iprec2 == iprec || iprec == i)
		return (0);
	return (1);
}

int				ft_square_init(char *str, t_obj *ret)
{
	int i;
	int iprec;
	int iprec2;

	i = 0;
	printf("square\n");
	i += ft_get_vec(str, &ret->coor);
	iprec2 = i;
	i += ft_get_vec(&str[i], &ret->ori);
	iprec = i;
	i += atof_point(&str[i], &ret->hei);
	if (!(ft_get_color(&str[i], &ret->color)))
		return (0);
	if (!iprec2 || iprec2 == iprec || iprec == i)
		return (0);
	return (1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgelinet <lgelinet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/25 16:10:11 by lgelinet          #+#    #+#             */
/*   Updated: 2021/03/05 17:21:27 by lgelinet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minirt.h"

int				ft_general_init(char *str, t_img *img)
{
	if (str[0] == 'R')
		return (ft_get_res(&str[1], img));
	else if (str[0] == 'A')
		return (ft_get_amb(&str[1], &img->amb));
	else if (str[0] == 'c')
		return (ft_get_cam(&str[1], &img->cam));
	else if (str[0] == 'l')
		return (ft_get_light(&str[1], &img->light));
	else
		return (0);
}

int				ft_new_obj(char *str, t_img *img)
{
	t_obj	*ret;
	int		msg;

	ret = (t_obj *)malloc(sizeof(t_obj));
	ret->next = img->objptr;
	msg = 0;
	ft_strlcpy(ret->id, str, 3);
	if (str[0] == 'p' && str[1] == 'l')
		msg = ft_plane_init(&str[2], ret);
	else if (str[0] == 's' && str[1] == 'q')
		msg = ft_square_init(&str[2], ret);
	else if (str[0] == 'c' && str[1] == 'y')
		msg = ft_cylinder_init(&str[2], ret);
	else if (str[0] == 't' && str[1] == 'r')
		msg = ft_triangle_init(&str[2], ret);
	else if (str[0] == 's' && str[1] == 'p')
		msg = ft_sphere_init(&str[2], ret);
	if (!msg)
	{
		free(ret);
		return (0 * ft_putstr_fd("ERROR IN OBJ INIT\n", 1));
	}
	img->objptr = ret;
	return (1);
}

int				parse_img(int fd, t_img **ret)
{
	char	*tofree;
	int		msg;
	int		retint;

	retint = 1;
	(*ret)->objptr = NULL;
	while (retint && ((retint = get_next_line(fd, &tofree)) > -1))
	{
		if (tofree[0] == ' ' || !tofree[0] || tofree[0] == '\t')
			msg = 1;
		else if (ft_strrchr("RAcl", tofree[0]) && ft_strrchr(" \t", tofree[1]))
		{
			if (!ft_general_init(tofree, *ret))
				msg = 0 * ft_putstr_fd("ERROR IN IMG PARSE/ generic_variables\n", 1);
		}
		else if (ft_strrchr("spct", tofree[0]) && ft_strrchr("plqyr", tofree[1]))
			msg = ft_new_obj(tofree, *ret) ? 0 * ft_putstr_fd("objerror", 1) : 1;
		else
			msg = 0 * ft_putstr_fd("ERROR in parse FORMAT", 1);
		free(tofree);
		if (!msg)
			break;
	}
	return (msg);
}
